## Why don't you start writing tests?

*"Let today be the start of something new."*

***William Shakespeare***

### Tutorial

From [laravel.io](https://laravel.io/articles/why-dont-you-start-writing-tests).

```bash
php artisan test
```

### License

Licensed under the [MIT license](https://opensource.org/licenses/MIT).
