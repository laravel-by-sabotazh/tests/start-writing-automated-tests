<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\LazilyRefreshDatabase;
use Tests\TestCase;

class UserTest extends TestCase
{
    use LazilyRefreshDatabase;

//    /**
//     * A basic feature test example.
//     *
//     * @return void
//     */
//    public function test_example(): void
//    {
//        $response = $this->get('/users');
//
//        $response->assertStatus(200);
//    }

    /**
     * A feature test example.
     *
     * @return void
     */
    public function test_new_user_can_be_added(): void
    {
        $response = $this->post('/users', [
            'name' => 'John',
            'email' => 'john.doe@example.com',
            'password' => 'Qwerty123$',
            'password_confirmation' => 'Qwerty123$',
        ]);

        $response->assertRedirect('/')
            ->assertSessionHas('success', 'User created successfully.');

        $this->assertDatabaseHas('users', [
            'name' => 'John',
            'email' => 'john.doe@example.com',
        ]);
    }

    /**
     * A feature test example.
     *
     * @return void
     */
    public function test_new_user_data_is_validated(): void
    {
        $response = $this->post('/users', [
            'name' => '',
            'email' => 'not_email_format',
            'password' => 'Qwerty123$',
            'password_confirmation' => 'not_Qwerty123$',
        ]);

        $response->assertStatus(302)
            ->assertSessionHasErrors(['name', 'email', 'password']);
    }

    /**
     * A feature test example.
     *
     * @return void
     */
    public function test_new_user_with_same_email_cannot_be_added(): void
    {
        User::factory()->create([
            'email' => 'john.doe@example.com',
        ]);

        $response = $this->post('/users', [
            'name' => 'John',
            'email' => 'john.doe@example.com',
            'password' => 'Qwerty123$',
            'password_confirmation' => 'Qwerty123$',
        ]);

        $response->assertStatus(302)
            ->assertSessionDoesntHaveErrors(['name', 'password'])
            ->assertSessionHasErrors(['email']);
    }
}
